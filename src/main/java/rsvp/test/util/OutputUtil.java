package rsvp.test.util;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.stream.Collectors;

import rsvp.test.model.Event;
import rsvp.test.model.Root;

public class OutputUtil {

	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	/**
	 * 
	 * @param root
	 * @param sortEventList
	 */
    public static void buildEvent (Root root, SortedSet<Event> sortEventList) {
    	sortEventList.add(root.getEvent());
    }
    
    /**
     * RSVP's received per host country.  
     * @param root
     * @param map
     */
    public static void buildTop3RsvpPerEventInCountry (Root root, Map<String, Integer> map) {
    	if ( null == map.get(root.getGroup().getGroup_country())){
    		map.put(root.getGroup().getGroup_country(), root.getGuests());
    	}else {
    		map.put(root.getGroup().getGroup_country(), 
    				map.get(root.getGroup().getGroup_country()) + root.getGuests());
    	}
    }
    
    public static Map<String, Integer> findTop3RsvpPerCountry (Map<String, Integer> map) {
    	Map<String, Integer> descending = map
					    	.entrySet()
					        .stream()
					        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
					        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (value1, value2) -> value2, LinkedHashMap::new));
    	return descending;

    }
    
    public static String printOutput (int totalCount, SortedSet<Event> sortEventList, Map<String, Integer> map, int top){
        Event furthestEvent = sortEventList.last();
        
        Map<String, Integer> mapDescending = findTop3RsvpPerCountry (map);
        int count = 1;
        StringBuffer resultBuffer = new StringBuffer();
        String key;
        
        for (Map.Entry<String, Integer> entry : mapDescending.entrySet()) {
        	resultBuffer.append(entry.getKey()).append(",").append(mapDescending.get(entry.getKey()));
        	
        	if (count < top) {
        		resultBuffer.append(",");
        		count++;
        	}else {
        		break;
        	}
        }
        
        return (totalCount + "," + dateFormat.format(new Date(furthestEvent.getTime())) + "," + furthestEvent.getEvent_url() + ","  + resultBuffer);

    }
}
