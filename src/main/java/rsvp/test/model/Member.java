package rsvp.test.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Member {

    private int member_id;
    private String photo;
    private String member_name;
    @JsonIgnore
    public String other_services;
    
	public int getMember_id() {
		return member_id;
	}
	public void setMember_id(int member_id) {
		this.member_id = member_id;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getMember_name() {
		return member_name;
	}
	public void setMember_name(String member_name) {
		this.member_name = member_name;
	}
	public String getOther_services() {
		return other_services;
	}
	public void setOther_services(String other_services) {
		this.other_services = other_services;
	}
    
}
