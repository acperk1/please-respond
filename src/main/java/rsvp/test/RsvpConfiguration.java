package rsvp.test;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan
@PropertySource("classpath:application.properties")
public class RsvpConfiguration {

	protected Logger logger;
    
	@Value("${meetup.url}")
    public String meetupUrl;
	
	@Value("${url.timer}")
    public String urlTimer;	
    
	@Value("${top.list.count}")
    public int top;
	
	private int urlTimerInt = 0;
	
	public RsvpConfiguration() {
		logger = Logger.getLogger(getClass().getName());
	}

	public int getUrlTimerInt() {
		setUrlTimerInt (urlTimer);
		return urlTimerInt;
	}

	public void setUrlTimerInt(String urlTimerInt) {
		this.urlTimerInt = Integer.parseInt(urlTimerInt);
	}
}
