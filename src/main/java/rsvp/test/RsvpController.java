package rsvp.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import rsvp.test.model.Event;
import rsvp.test.model.Root;
import rsvp.test.util.OutputUtil;

@RestController
public class RsvpController {

	@Autowired
	protected RestTemplate restTemplate;
	
	@Autowired
	protected RsvpConfiguration config;
	
    @RequestMapping(value = "/rsvp/test", method = RequestMethod.GET, produces = "application/json")
    public String getRsvp() throws Exception {
    	return callMeetUp();
    }
    

    public static void main(String[] args) throws Exception {
    	new RsvpController ().callMeetUp();
    }
    
    public String callMeetUp() {
    	BufferedReader responseStream = null;
    	String output = "";
    	try {
	        URL meetUp = new URL(config.meetupUrl);
	        URLConnection meetUpConnection = meetUp.openConnection();
	        responseStream = new BufferedReader(new InputStreamReader(meetUpConnection.getInputStream()));
	        String response;
	        long startTime = System.currentTimeMillis();
	        int totalCount = 0;
	        ObjectMapper mapper = new ObjectMapper();
	        SortedSet<Event> sortEventList = new TreeSet<Event>();
	        Map<String, Integer> map = new HashMap<String, Integer>();
	        while ((response = responseStream.readLine()) != null && System.currentTimeMillis()-startTime <= config.getUrlTimerInt()) {
	        	Root result = mapper.readValue(response, Root.class);
	        	OutputUtil.buildEvent (result, sortEventList);
	        	OutputUtil.buildTop3RsvpPerEventInCountry (result, map);
	        	totalCount++;
	        }
	        output = OutputUtil.printOutput(totalCount, sortEventList, map, config.top);
    	}catch (IOException exp) {
    		exp.printStackTrace();
    	}finally {
    		if ( null != responseStream ) {
    			try {
					responseStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    		}
    	}
    	return output;
    }
    

    

}
