package rsvp.test;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@Import(RsvpConfiguration.class)
public class App  implements CommandLineRunner {
	
	protected Logger logger = Logger.getLogger(getClass().getName());
	
	@Autowired
	protected RsvpConfiguration config;
	
	@Autowired
	protected RsvpController controller;


    public static void main(String[] args) {
    	System.setProperty("spring.config.name", "rsvp-server");
        SpringApplication.run(App.class, args);
    }
    
    @Override
    public void run(String... args) {
    	logger.info("**************************************Output for Enlighten-challenge/please-respond********************************");
    	logger.info(controller.callMeetUp());
    	logger.info("*******************************************************************************************************************");
    }
    
    @Bean
    RestTemplate restTemplate() {
    	RestTemplate rt = new RestTemplate();
    	HttpComponentsClientHttpRequestFactory httpClient = new HttpComponentsClientHttpRequestFactory();
        return rt;
    }

}
