# Enlighten-challenge/please-respond Excercise

## Why I picked this exercise
* I thought this exercise gives an opportunity to share my logical ability to build the code and also to showcase my experience with Spring Boot, Rest API (Microservices), Docker, and Maven 

## Technology Stack Used 
* Java 1.8
* Spring Boot
* Rest API
* Maven
* YAML
* Docker

## Approach and Assumptions
* This is a Web application built using Spring Boot  
* Initially I thought that I can simply use a resttemplate to access the Meetup API, but that become a problem once I realized that Meetup API was slow and I need to stream the response with a time limit (set as 60 seconds in application.properties file). So I used the Java net and streamed the I/O with a time limit on it.  


## Build the application

```
mvn clean install
```
## Run Using Docker

## Build the image

```
docker build -t please-respond .
```

## Run the image locally

```
docker run --name please-respond -p 2222:2222 please-respond java -jar app.jar App
```
* As soon as you run using Docker container from the command line, the output will be automatically displayed after 60 seconds.
* If you want to re-run it again, then please use a browser or postman using the link http://localhost:2222/rsvp/test

## NOTE:  If you try to run the app again without closing the previous instance, then you will get a port binding error.

## Run the code using Maven
* If you want to execute using Maven (Maven), just type the command 

```
mvn exec:java -Dexec.mainClass="rsvp.test.App"
```
## Run the code using Eclipse or simple commandline using Java
* If you are using Eclipse IDE, after the build simply run the App.java file.  
* If you want to execute from command prompt, then go to the project folder and run 

```
java rsvp.test.App
```
* If you want to re-run it again, then please use a browser or postman  using the link http://localhost:2222/rsvp/test

## Example output of the test case

```
87,2020-11-10 14:30,https://www.meetup.com/product-management-seattle/events/273272168/,us,7,de,0,in,0
```

## Notes and room for improvement
* Please note that I did not have the time to complete the JUNIT test cases for this.  I would be more than happy to walk over anything that I have missed.
* I think we can use some new streaming methods (Spring StreamResponse, or something similar) instead of java.net packages.  
* Improve Error and exception handling.
* Add comments for methods and classes.
* Better naming conventions
