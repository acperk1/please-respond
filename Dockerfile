FROM openjdk:8-jre
ADD target/please-respond-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 2222
CMD ["java", "-version"]
ENTRYPOINT ["java","-jar","/app.jar","App"]
